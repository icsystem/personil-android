package com.ics.personil;

/**
 * Created by rahardyan on 05/09/16.
 */
public class UserLocation {
    private final float lat, lng;

    public UserLocation(float lat, float lng) {
        this.lat = lat;
        this.lng = lng;
    }

    public float getLat() {
        return lat;
    }

    public float getLng() {
        return lng;
    }
}
