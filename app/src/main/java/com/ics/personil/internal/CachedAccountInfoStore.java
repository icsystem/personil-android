package com.ics.personil.internal;

import android.content.Context;
import android.content.SharedPreferences;

import com.ics.personil.pojo.AccountInfo;
import com.ics.personil.pojo.UserChannelToken;

public class CachedAccountInfoStore {
    private static final String TAG = CachedAccountInfoStore.class.getSimpleName();

    private final SharedPreferences sharedPreferences;

    public CachedAccountInfoStore(Context context){
        this.sharedPreferences = context.getSharedPreferences(TAG,Context.MODE_PRIVATE);
    }

    public boolean containsAccountInfo(){
        return sharedPreferences.contains("id")
                && sharedPreferences.contains("nrp_number")
                && sharedPreferences.contains("fcm_token");
    }

    public SharedPreferences getSharedPreferences() {
        return sharedPreferences;
    }

    public void cahceAccountInfo(AccountInfo accountInfo){
        SharedPreferences.Editor editor =sharedPreferences.edit();
        editor.putString("id",accountInfo.id);
        editor.putString("name", accountInfo.name);
        editor.putString("format_name", accountInfo.formatName);
        editor.putString("nrp_number", accountInfo.nrpNumber);
        editor.putString("phone_number", accountInfo.phoneNumber);
        editor.putString("channel", accountInfo.channel);
        editor.putString("online_status", accountInfo.onlineStatus);
        editor.putString("online_status_string", accountInfo.onlineStatusString);
        editor.putString("job_id", accountInfo.getJobPosition().getId());
        editor.putString("job_title", accountInfo.getJobPosition().getTitle());
        editor.putInt("update_interval", accountInfo.setting.getUpdateInterval());
        editor.putInt("radius", accountInfo.setting.getRadius());
        editor.putBoolean("is_monitoring", accountInfo.setting.isMonitoring());
        editor.apply();
    }
    public void cacheTokenOnly(String authenticationToken){
        SharedPreferences.Editor editor =sharedPreferences.edit();
        editor.putString("authentication_token",authenticationToken);
        editor.apply();
    }
    public void cacheUserToken(UserChannelToken userChannelToken){
        SharedPreferences.Editor editor =sharedPreferences.edit();
        editor.putString("authentication_token",userChannelToken.getAuthenticationToken());
        editor.putString("channel",userChannelToken.getChannel());
        editor.putString("fcm_token", userChannelToken.getIidToken());
        editor.apply();
    }

    public UserChannelToken getUserChannelToken(){
        String authToken =  sharedPreferences.getString("authentication_token", "");
        String channel = sharedPreferences.getString("channel", "");
        String tokenFCM = sharedPreferences.getString("fcm_token", "");

        return new UserChannelToken(authToken,channel,tokenFCM);
    }

    public void cacheTokenFCMOnly(String authenticationToken){
        SharedPreferences.Editor editor =sharedPreferences.edit();
        editor.putString("fcm_token",authenticationToken);
        editor.apply();
    }

    public String getTokenFCM(){
        return sharedPreferences.getString("fcm_token","");
    }

    public String getChannel(){
        return sharedPreferences.getString("channel", "");
    }

    public void saveChannel(String channel){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("channel", channel);
        editor.apply();
    }

    public String getToken(){
        return sharedPreferences.getString("authentication_token", "");
    }

    public AccountInfo getCachcedAccountInfo(){
        if(!containsAccountInfo()){
            throw new RuntimeException("There is no cached AccountInfo yet");
        }

        String id = sharedPreferences.getString("id", "");
        String name = sharedPreferences.getString("name", "");
        String formatName = sharedPreferences.getString("format_name", "");
        String nrpNumber = sharedPreferences.getString("nrp_number", "");
        String phoneNumber = sharedPreferences.getString("phone_number", "");
        String channel = sharedPreferences.getString("channel", "");
        String onlineStatus = sharedPreferences.getString("online_status", "");
        String onlineStatusString = sharedPreferences.getString("online_status_string", "");
        String jobId = sharedPreferences.getString("job_id", "");
        String jobTitle = sharedPreferences.getString("job_title", "");
        int updateInterval = sharedPreferences.getInt("update_interval", 5);
        int radius = sharedPreferences.getInt("radius", 5);
        boolean isMonitoring = sharedPreferences.getBoolean("is_monitoring", false);

        return new AccountInfo(id, name, formatName, nrpNumber, phoneNumber, channel, onlineStatus, onlineStatusString, jobId, jobTitle,
                updateInterval, radius, isMonitoring);
    }

    public void clear(){
        sharedPreferences.edit().clear().apply();
    }
}
