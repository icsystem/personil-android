package com.ics.personil.internal;

import android.content.Context;
import android.content.SharedPreferences;

import com.ics.personil.R;

/**
 * Created by rahardyan on 27/08/16.
 */
public class CacheManager {
    private final SharedPreferences sharedPreferences;
    private Context context;

    public CacheManager(Context context) {
        this.sharedPreferences = context.getSharedPreferences("cache manager",Context.MODE_PRIVATE);
        this.context = context;
    }

    public void saveTokenFCM(String tokenFcm){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("token_fcm", tokenFcm);
        editor.apply();
    }

    public String getTokenFCM(){
        String tokenFcm = sharedPreferences.getString("token_fcm", "");
        return tokenFcm;
    }

    public void clear() {
        sharedPreferences.edit().clear().apply();
    }

    public void setEndpointUrl(String url){
        sharedPreferences.edit().putString("api_url", url).apply();
    }

    public String getEndpointUrl(){
        return sharedPreferences.getString("api_url", context.getString(R.string.production_api));
    }
}
