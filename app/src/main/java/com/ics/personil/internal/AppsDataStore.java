package com.ics.personil.internal;

import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;

import com.ics.personil.TraficQuickResponseApplication;
import com.ics.personil.UserEvent;
import com.ics.personil.network.TraficQuickResponseService;

import org.greenrobot.eventbus.EventBus;

import rx.Observable;
import rx.Subscriber;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by angga on 8/29/16.
 */
public class AppsDataStore {
    public static final String TAG = AppsDataStore.class.getSimpleName();

    private final TraficQuickResponseService traficQuickResponseService;
    private final CachedAccountInfoStore cachedAccountInfoStore;
    private final CacheManager cacheManager;

    public AppsDataStore(TraficQuickResponseService traficQuickResponseService, CachedAccountInfoStore cachedAccountInfoStore, CacheManager cacheManager) {
        this.traficQuickResponseService = traficQuickResponseService;
        this.cachedAccountInfoStore = cachedAccountInfoStore;
        this.cacheManager = cacheManager;
    }

    public Observable<Void> logout(){
        return Observable.create(new Observable.OnSubscribe<Void>() {
            @Override
            public void call(Subscriber<? super Void> subscriber) {
                subscriber.onNext(null);
                subscriber.onCompleted();
            }
        }).flatMap(new Func1<Void, Observable<Void>>() {
            @Override
            public Observable<Void> call(Void aVoid) {
                return traficQuickResponseService.logout()
                        .subscribeOn(Schedulers.io())
                        .onErrorReturn(new Func1<Throwable, Void>() {
                            @Override
                            public Void call(Throwable throwable) {
                                throwable.printStackTrace();
                                return null;
                            }
                        });
            }
        }).doOnNext(new Action1<Void>() {
            @Override
            public void call(Void aVoid) {
                Log.d(TAG, "call: ctr " + cachedAccountInfoStore.getToken() + " fcm " +  cacheManager.getTokenFCM());
                EventBus.getDefault().post(UserEvent.LOGOUT);
                cachedAccountInfoStore.clear();
                Log.d(TAG, "call: ctr " + cachedAccountInfoStore.getToken() + " fcm " + cacheManager.getTokenFCM());
                NotificationManagerCompat.from(TraficQuickResponseApplication.getInstance().getApplicationContext()).cancelAll();
            }
        });
    }
}
