package com.ics.personil.internal;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.ics.personil.TraficQuickResponseApplication;
import com.ics.personil.network.TraficQuickResponseService;

import javax.inject.Inject;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by rahardyan on 10/09/16.
 */
public class SendLocationBroadcastRecevier extends BroadcastReceiver{
    double lat, lng;
    @Inject
    TraficQuickResponseService traficQuickResponseService;

    @Override
    public void onReceive(Context context, Intent intent) {
        TraficQuickResponseApplication.getComponent().inject(this);

        lat = intent.getDoubleExtra("latitude", -1);
        lng = intent.getDoubleExtra("longitude", -1);

        Log.d("newService", lat + ", " + lng);
        updateLocation((float) lat, (float) lng);

    }

    private void updateLocation(final float lat, final float lng){
        if (lat == -1){
            return;
        }
        traficQuickResponseService.pushLocation(lat, lng)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<Void>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("amsibsam: sendLocation", e.toString());
                    }

                    @Override
                    public void onNext(Void aVoid) {
                        Log.e("amsibsam: sendLocation", lat + ", " +lng);
                    }
                });
    }
}
