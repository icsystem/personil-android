package com.ics.personil.network;

import android.app.Service;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.os.IBinder;
import android.os.PowerManager;
import android.support.annotation.Nullable;
import android.util.Log;
import android.util.Pair;

import com.google.gson.JsonObject;
import com.ics.personil.TraficQuickResponseApplication;
import com.ics.personil.UserEvent;
import com.ics.personil.UserLocation;
import com.ics.personil.internal.CacheManager;
import com.ics.personil.internal.CachedAccountInfoStore;
import com.ics.personil.ui.activity.NotificationActivity;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;

import javax.inject.Inject;

import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by rahardyan on 26/08/16.
 */
public class PusherNotificationService extends Service implements LocationListener {
    private static final String TAG = PusherNotificationService.class.getSimpleName();
    private Subscription pusherEvent;
    @Inject
    PusherService pusherService;

    @Inject
    CacheManager cacheManager;

    @Inject
    CachedAccountInfoStore cachedAccountInfoStore;

    public PusherNotificationService() {

    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        TraficQuickResponseApplication.getComponent().inject(this);
        if (!EventBus.getDefault().isRegistered(this)) {
            Log.d(TAG, "onCreate: ctr event bus register ");
            EventBus.getDefault().register(this);
        }
        listenPusherEvent();

        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                Log.d("amsibsam", "push online status");
//                pusherService.pushOnlineStatus();

            }
        }, 10);


    }

    private void listenPusherEvent() {
        Log.d("amsibsam", "channel: "+ cachedAccountInfoStore.getChannel());
        pusherEvent = pusherService.getChannelEvents(cachedAccountInfoStore.getChannel())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Pair<PusherService.ChannelEvent, JsonObject>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(Pair<PusherService.ChannelEvent, JsonObject> channelEventJsonObjectPair) {
                        Log.d("amsibsam", "event type : "+ channelEventJsonObjectPair.first.name());
                        Log.d("ctr ", "message : "+ channelEventJsonObjectPair.second.toString());
                        try {
                            JSONObject rawJsonData = new JSONObject(channelEventJsonObjectPair.second.toString());
                            JSONObject data = rawJsonData.getJSONObject("data");
                            JSONObject openCase = data.getJSONObject("open_case");
                            String description = openCase.getString("description");
                            String location = openCase.getString("location");
                            String latitude = openCase.getString("lat");
                            String longitude = openCase.getString("lng");
                            String id = openCase.getString("id");

                            PowerManager.WakeLock screenOn = ((PowerManager)getSystemService(POWER_SERVICE)).newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "example");
                            screenOn.acquire();

                            Intent intent = NotificationActivity.generateNotificationActivity(getApplicationContext(), channelEventJsonObjectPair.first.name(),description, latitude, longitude, location, id);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            getApplicationContext().startActivity(intent);
                            screenOn.release();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

    }

    @Subscribe
    public void onUserEvent(UserEvent userEvent){
        Log.d("amsibsam", "event catched");
        switch (userEvent) {
            case LOGIN:
                if (pusherEvent == null || pusherEvent.isUnsubscribed()) {
                     listenPusherEvent();
                }
                break;
            case LOGOUT:
                Log.d(TAG, "onUserEvent: " + pusherEvent.isUnsubscribed());
                if (pusherEvent != null && !pusherEvent.isUnsubscribed()) {
                    Log.d(TAG, "onUserEvent: ctr enter unsubscribe");
                    pusherEvent.unsubscribe();
                    stopService(new Intent(this, PusherNotificationService.class));
                    pusherService.unsubscribe(cachedAccountInfoStore.getChannel());
                }
                break;
        }
    }

    @Subscribe
    public void onUserLocation(UserLocation userLocation){
//        pusherService.pushOnlineStatus();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d("amsibsam", "lat long "+location.getLatitude() + ", " + location.getLongitude());
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (pusherEvent == null || pusherEvent.isUnsubscribed()) {
            listenPusherEvent();
        }
        return Service.START_STICKY;
    }
}
