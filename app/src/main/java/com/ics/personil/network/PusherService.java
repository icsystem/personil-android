package com.ics.personil.network;

import android.util.Log;
import android.util.Pair;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.pusher.client.Pusher;
import com.pusher.client.PusherOptions;
import com.pusher.client.channel.ChannelEventListener;
import com.pusher.client.channel.PrivateChannel;
import com.pusher.client.channel.PrivateChannelEventListener;
import com.pusher.client.connection.ConnectionState;
import com.pusher.client.connection.ConnectionStateChange;
import com.pusher.client.util.HttpAuthorizer;

import java.util.HashMap;
import java.util.Map;

import rx.Observable;
import rx.functions.Action2;
import rx.subjects.PublishSubject;

/**
 * Created by rahardyan on 26/08/16.
 */
public class PusherService implements com.pusher.client.connection.ConnectionEventListener {
    private static final String TAG = PusherService.class.getSimpleName();
    final String PUSHER_KEY = "6f7c8897afcdaf895b9c";
    private static final Map<String, ChannelEvent> CHANNEL_EVENTS = new HashMap<>();
    private final PublishSubject<Pair<ChannelEvent, JsonObject>> channelPublishSubject;
    private final Pusher pusher;
    private final Gson gson;

    public PusherService() {
        HttpAuthorizer authorizer = new HttpAuthorizer("https://api-tqr.herokuapp.com");
        PusherOptions options = new PusherOptions().setAuthorizer(authorizer);
        pusher = new Pusher(PUSHER_KEY, options);

        gson = new GsonBuilder()
                .create();

        channelPublishSubject = PublishSubject.create();
    }

    public Observable<Pair<ChannelEvent, JsonObject>> getChannelEvents(String channelCode) {
        return startSubscribeChannel(channelCode);
    }

    public void unsubscribe(String channel){
        pusher.unsubscribe(channel);
    }

    public void pushOnlineStatus(){
        Log.d("amsibsam", "start publish online status");

        connectPusher();
        final PrivateChannel channel = pusher.getPrivateChannel("private-admin_traffic_QR");
        pusher.subscribePrivate("private-admin_traffic_QR", new PrivateChannelEventListener() {
            @Override
            public void onAuthenticationFailure(String s, Exception e) {
                Log.d("amsibsam", "auth fail "+s);
                e.printStackTrace();
                channel.trigger("client-location_status", "{data: {lat: \"34567\", lng: \"456789\"}}");
            }

            @Override
            public void onSubscriptionSucceeded(String s) {
                channel.trigger("client-location_status", "{data: {lat: \"34567\", lng: \"456789\"}}");
            }

            @Override
            public void onEvent(String s, String s1, String s2) {
                Log.d("amsibsam", "data "+s1);
            }
        });
    }

    public Observable<Pair<ChannelEvent, JsonObject>> startSubscribeChannel(String channelCode) {
        String[] channelEventNames = CHANNEL_EVENTS.keySet().toArray(new String[CHANNEL_EVENTS.size()]);
        startSubscribePusher(channelCode, channelEventNames, new Action2<String, String>() {
            @Override
            public void call(String event, String data) {
                ChannelEvent channelEvent = CHANNEL_EVENTS.get(event);
                JsonObject dataEvent = gson.fromJson(data, JsonObject.class);

                channelPublishSubject.onNext(new Pair<>(channelEvent, dataEvent));
            }
        });

        return channelPublishSubject.asObservable();
    }


    public void startSubscribePusher(String channel, String[] eventNames, final Action2<String, String> notifCallback){
        connectPusher();
        pusher.subscribe(channel, new ChannelEventListener() {
            @Override
            public void onSubscriptionSucceeded(String channel) {

            }

            @Override
            public void onEvent(String channel, String event, String data) {
                Log.d("PusherService", data);
                Log.d("PusherService", event);
                notifCallback.call(event, data);
            }
        }, eventNames);
    }

    private void connectPusher() {
        ConnectionState pusherConnectionState = pusher.getConnection().getState();
        if (!(pusherConnectionState.equals(ConnectionState.CONNECTED) ||
                pusherConnectionState.equals(ConnectionState.CONNECTING))) {
            pusher.connect(new com.pusher.client.connection.ConnectionEventListener() {
                @Override
                public void onConnectionStateChange(ConnectionStateChange connectionStateChange) {

                }

                @Override
                public void onError(String message, String code, Exception e) {
                    Log.e("PusherService", "error connect "+message);
                }
            }, ConnectionState.ALL);
        }
    }

    public enum ChannelEvent {
        KECELAKAAN,
        PLB,
        KEMACETAN,
        APEL_PERS,
        BANTUAN_MEDIS,
        KEJAHATAN,
        BENCANA_KEBAKARAN
    }


    @Override
    public void onConnectionStateChange(ConnectionStateChange connectionStateChange) {

    }

    @Override
    public void onError(String s, String s1, Exception e) {

    }

    static {
        CHANNEL_EVENTS.put("kecelakaan", ChannelEvent.KECELAKAAN);
        CHANNEL_EVENTS.put("kemacetan", ChannelEvent.KEMACETAN);
        CHANNEL_EVENTS.put("plb", ChannelEvent.PLB);
        CHANNEL_EVENTS.put("apel_pers", ChannelEvent.APEL_PERS);
    }
}
