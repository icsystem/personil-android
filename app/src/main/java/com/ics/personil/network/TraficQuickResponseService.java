package com.ics.personil.network;

import android.content.Context;
import android.util.Log;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ics.personil.internal.CacheManager;
import com.ics.personil.internal.CachedAccountInfoStore;
import com.ics.personil.pojo.AccountInfo;
import com.ics.personil.pojo.OpenCase;
import com.ics.personil.pojo.UserChannelToken;
import com.ics.personil.ui.utils.response.ListResponse;
import com.ics.personil.ui.utils.response.Response;
import com.squareup.okhttp.HttpUrl;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.logging.HttpLoggingInterceptor;

import java.util.List;
import java.util.concurrent.TimeUnit;

import retrofit.BaseUrl;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;
import retrofit.RxJavaCallAdapterFactory;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.POST;
import retrofit.http.PUT;
import rx.Observable;
import rx.functions.Func1;

/**
 * Created by rahardyan on 25/08/16.
 */
public class TraficQuickResponseService {

    private static final String TAG = TraficQuickResponseService.class.getSimpleName();

    private interface TraficQuickResponseApi {

        @FormUrlEncoded
        @POST("users/session/sign_in")
        Observable<Response> login(@Field("nrp_number") String nrpNumber,
                                   @Field("password") String password,
                                   @Field("iid_token") String iidToken);

        @FormUrlEncoded
        @POST("users/online_status")
        Observable<Void> setStatus(@Header("Authorization") String token,
                                   @Field("online_status") String onlineStatus);

        @GET("users/current")
        Observable<Response> accountInfo(@Header("Authorization") String token);

        @FormUrlEncoded
        @PUT("/users/location")
        Observable<Void> pushLocation(@Header("Authorization") String token,
                                      @Field("lat") float lat,
                                      @Field("lng") float lng);

        @POST ("/users/session/sign_out")
        Observable<Void> logout(@Header("Authorization") String token);

        @GET("/open_cases")
        Observable<ListResponse<OpenCase>> getOpenCases(@Header("Authorization") String token);

        @FormUrlEncoded
        @PUT("/open_cases/readed")
        Observable<Void> readCase(@Header("Authorization") String token,
                                  @Field("id") String caseId);

        @FormUrlEncoded
        @PUT("open_cases/checkin")
        Observable<Void> checkIn(@Header("Authorization") String token,
                                 @Field("id") String caseId,
                                 @Field("description") String description);

    }

    private TraficQuickResponseApi traficQuickResponseApi;
    private CachedAccountInfoStore cachedAccountInfoStore;
    private CacheManager cacheManager;

    private Gson gson = new GsonBuilder()
            .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
            .create();

    public TraficQuickResponseService(Context context, final CachedAccountInfoStore cachedAccountInfoStore,
                                      final CacheManager cacheManager) {
        this.cachedAccountInfoStore = cachedAccountInfoStore;
        this.cacheManager = cacheManager;
        // 1. Prepare the baseUrl. We will dynamically resolve this from the SharedPreferences.
        final BaseUrl baseUrl = new BaseUrl() {
            @Override
            public HttpUrl url() {
                final String baseUrl = cacheManager.getEndpointUrl();
                return HttpUrl.parse(baseUrl);
            }
        };

        // 2. Create and prepare the Client that will be the "backend".
        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(120, TimeUnit.SECONDS);
        client.setReadTimeout(120, TimeUnit.SECONDS);
        //hide temporary for handle bad request 400

//        client.networkInterceptors().add(new Interceptor() {
//            @Override
//            public com.squareup.okhttp.Response intercept(Chain chain) throws IOException {
//                Request.Builder builder = chain.request().newBuilder();
//                Log.d(TAG, "intercept: " + cachedAccountInfoStore.containsAccountInfo());
//                if(cachedAccountInfoStore.containsAccountInfo()){
//                    builder.addHeader("Authorization",cachedAccountInfoStore.getUserChannelToken().getAuthenticationToken());
//                    Log.d(TAG, "BaseUrl: " + baseUrl.url().toString());
//                    Log.d(TAG, "UiidToken: " + cachedAccountInfoStore.getTokenFCM());
//                    Log.d(TAG, "Token: " + cachedAccountInfoStore.getUserChannelToken().getAuthenticationToken());
//                }
//                return chain.proceed(builder.build());
//            }
//        });
        // 3. Create and prepare the logging interceptor. This is mainly for debugging purpose.
        // TBD: Is it better to use Stetho instead?
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
            @Override
            public void log(String message) {
                Log.d("retrofit", message);
            }
        });
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        client.interceptors().add(loggingInterceptor);

        // 4. Almost done. Now, we can create the Retrofit instance.
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(client)
                .build();

        // 5. Finally, we can create the model of the API.
        traficQuickResponseApi = retrofit.create(TraficQuickResponseApi.class);
    }

    public Observable<UserChannelToken> login(final String nrp, final String password) {
        Log.d(TAG, "login: ctr " + cacheManager.getTokenFCM());
        Log.d("amsibsam", "endpoint url "+cacheManager.getEndpointUrl());
        return traficQuickResponseApi.login(nrp, password,cacheManager.getTokenFCM())
                .map(new Func1<Response, UserChannelToken>() {
                    @Override
                    public UserChannelToken call(Response response) {
                        return response.getResult(UserChannelToken.class);
                    }
                });
    }

    public Observable<AccountInfo> getAccount() {
        return traficQuickResponseApi.accountInfo(cachedAccountInfoStore.getUserChannelToken().getAuthenticationToken())
                .map(new Func1<Response, AccountInfo>() {
                    @Override
                    public AccountInfo call(Response response) {
                        return response.getResult(AccountInfo.class);
                    }
                })
                .map(new Func1<AccountInfo, AccountInfo>() {
                    @Override
                    public AccountInfo call(AccountInfo accountInfo) {
                        return accountInfo;
                    }
                });
    }

    public Observable<Void> setOnlineStatus(String onlineStatus) {
        return traficQuickResponseApi.setStatus(cachedAccountInfoStore.getUserChannelToken().getAuthenticationToken(),onlineStatus)
                .map(new Func1<Void, Void>() {
                    @Override
                    public Void call(Void aVoid) {
                        return aVoid;
                    }
                });
    }

    public Observable<Void> pushLocation(float lat, float lng){
        return traficQuickResponseApi.pushLocation(cachedAccountInfoStore.getUserChannelToken().getAuthenticationToken(),lat, lng)
                .map(new Func1<Void, Void>() {
                    @Override
                    public Void call(Void aVoid) {
                        return aVoid;
                    }
                });
    }
    public Observable<Void> logout(){
        return traficQuickResponseApi.logout(cachedAccountInfoStore.getUserChannelToken().getAuthenticationToken())
                .map(new Func1<Void, Void>() {
                    @Override
                    public Void call(Void aVoid) {
                        return null;
                    }
                });
    }
    public Observable<List<OpenCase>> getOpenCases(){
        return traficQuickResponseApi.getOpenCases(cachedAccountInfoStore.getUserChannelToken().getAuthenticationToken())
                .map(new Func1<ListResponse<OpenCase>, List<OpenCase>>() {
                    @Override
                    public List<OpenCase> call(ListResponse<OpenCase> openCaseListResponse) {
                        return openCaseListResponse.getResults(OpenCase.class);
                    }
                });
    }

    public Observable<Void> readCase(String id){
        return traficQuickResponseApi.readCase(cachedAccountInfoStore.getUserChannelToken().getAuthenticationToken(), id)
                .map(new Func1<Void, Void>() {
                    @Override
                    public Void call(Void aVoid) {
                        return null;
                    }
                });
    }

    public Observable<Void> checkIn(String caseId,String description){
        return traficQuickResponseApi.checkIn(cachedAccountInfoStore.getUserChannelToken().getAuthenticationToken(),
                caseId,description)
                .map(new Func1<Void, Void>() {
                    @Override
                    public Void call(Void aVoid) {
                        return null;
                    }
                });
    }

}
