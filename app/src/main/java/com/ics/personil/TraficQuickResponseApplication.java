package com.ics.personil;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.multidex.MultiDex;
import com.crashlytics.android.Crashlytics;
import com.ics.personil.internal.CacheManager;

import javax.inject.Inject;

import io.fabric.sdk.android.Fabric;

/**
 * Created by rahardyan on 25/08/16.
 */
public class TraficQuickResponseApplication extends Application {
    private static TraficQuickResponseApplication instance;
    private TraficQuickResponseComponent component;
    private SharedPreferences sharedPreferencesFirebase;
    private static CacheManager mCacheManager;

    @Inject CacheManager cacheManager;


    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());

        instance = this;
        component = TraficQuickResponseComponent.Initializer.init(this);
        sharedPreferencesFirebase = this.getSharedPreferences("firebase",Context.MODE_PRIVATE);

        TraficQuickResponseApplication.getComponent().inject(this);

        mCacheManager = cacheManager;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public static TraficQuickResponseApplication getInstance() {
        return instance;
    }

    public static TraficQuickResponseComponent getComponent() {
        return instance.component;
    }

    public static CacheManager getCacheManager(){
        return mCacheManager;
    }

    public static SharedPreferences getSharedPreferencesFirebase() {
        return instance.sharedPreferencesFirebase;
    }
}
