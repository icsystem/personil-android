package com.ics.personil.ui.utils;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.ics.personil.R;
import com.ics.personil.TraficQuickResponseApplication;
import com.ics.personil.UserEvent;
import com.ics.personil.internal.LocationService;
import com.ics.personil.ui.activity.HomeActivity;
import com.ics.personil.ui.activity.LoginActivity;
import com.ics.personil.ui.activity.OpenCaseActivity;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;

import org.greenrobot.eventbus.EventBus;

import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by rahardyan on 19/10/16.
 */

public class DrawerHelper {
    private Drawer drawer;

    public void initDrawer(final Activity activity, int selectedPosition){
        PrimaryDrawerItem item1 = new PrimaryDrawerItem().withIdentifier(1).withName("Beranda").withIcon(R.drawable.ic_home_grey_800_24dp);
        PrimaryDrawerItem item2 = new PrimaryDrawerItem().withIdentifier(2).withName("Daftar Kejadian").withIcon(R.drawable.ic_daftar_kejadian);

        drawer = new DrawerBuilder()
                .withActivity(activity)
                .addDrawerItems(
                        item1,
                        item2
                )
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        Log.d("amsibsam", "item position "+drawerItem.getIdentifier());
                        switch ((int) drawerItem.getIdentifier()){
                            case 1:
                                activity.startActivity(new Intent(activity, HomeActivity.class));
                                activity.finish();
                                break;
                            case 2:
                                activity.startActivity(new Intent(activity, OpenCaseActivity.class));
                                activity.finish();
                                break;
                            case 0:
                                activity.startActivity(new Intent(activity, HomeActivity.class).putExtra("logout", true));
                                activity.finish();
                                break;

                        }
                        return false;
                    }
                })
                .withSelectedItem(selectedPosition)
                .build();
        drawer.addStickyFooterItem(new PrimaryDrawerItem().withIdentifier(0).withName("Logout").withIcon(R.drawable.ic_logout_drawer));
    }

    public Drawer getDrawer(){
        return drawer;
    }


}
