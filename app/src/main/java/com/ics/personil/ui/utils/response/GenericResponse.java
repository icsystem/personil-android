package com.ics.personil.ui.utils.response;

import com.google.gson.JsonObject;

public class GenericResponse {
    protected JsonObject data;

    public JsonObject getData() {
        return data;
    }

    public void setData(JsonObject data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "GenericResponse{" +
                ", data=" + data +
                '}';
    }
}

