package com.ics.personil.ui.utils.response;

import com.ics.personil.ui.utils.JsonParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ListResponse<T> extends GenericResponse {
    private static final String TAG = ListResponse.class.getSimpleName();

    public List<T> getResults(Class<T> tClass) {

        try {
            List<T> results = new ArrayList<>();
            JSONObject dataJson = new JSONObject(data.toString());
            Iterator<?> keys = dataJson.keys();
            while (keys.hasNext()) {
                String key = (String) keys.next();
                JSONArray data = dataJson.getJSONArray(key);
                for (int i = 0; i < data.length(); i++) {
                    JSONObject object = data.getJSONObject(i);
                    results.add(JsonParser.getInstance().getParser().fromJson(object.toString(), tClass));
                }
            }
            return results;
        } catch (JSONException e) {
            throw new RuntimeException(e.getMessage());
        }
    }
}

