package com.ics.personil.ui.dialog;


import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;


import com.ics.personil.databinding.FragmentCheckInDilaogBinding;

/**
 * A simple {@link Fragment} subclass.
 */
public class CheckInDilaog extends DialogFragment {

    private FragmentCheckInDilaogBinding binding;

    private OnClickListener clickListener;
    private String caseId;

    public static CheckInDilaog newInstance(OnClickListener clickListener,String caseId) {
        CheckInDilaog click= new CheckInDilaog();
        click.clickListener = clickListener;
        click.caseId = caseId;
        return click;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentCheckInDilaogBinding.inflate(inflater,container,false);

        binding.buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        binding.buttonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (binding.editDescription.getText().toString().isEmpty()) {
                    binding.editDescription.setError("Mohon diisi deskripsinya");
                } else {
                    clickListener.onSubmit(binding.editDescription.getText().toString(),caseId, CheckInDilaog.this);
                }
            }
        });

        return binding.getRoot();
    }
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        }
    }

    public interface OnClickListener {
        void onSubmit(String description,String caseId, DialogFragment dialogFragment);
    }

}
