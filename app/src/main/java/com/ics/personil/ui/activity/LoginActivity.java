package com.ics.personil.ui.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.databinding.DataBindingUtil;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.os.EnvironmentCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.ics.personil.R;
import com.ics.personil.TraficQuickResponseApplication;
import com.ics.personil.UserEvent;
import com.ics.personil.databinding.ActivityLoginBinding;
import com.ics.personil.internal.CacheManager;
import com.ics.personil.internal.CachedAccountInfoStore;
import com.ics.personil.network.PusherNotificationService;
import com.ics.personil.network.TraficQuickResponseService;
import com.ics.personil.pojo.UserChannelToken;
import com.ics.personil.ui.utils.ConnectionDetector;
import com.ics.personil.ui.utils.DrawerHelper;
import com.ics.personil.ui.utils.FullScreenControl;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Inject;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class LoginActivity extends AppCompatActivity{
    private ActivityLoginBinding binding;
    private ProgressDialog progressDialog;
    private ConnectionDetector connectionDetector;
    @Inject
    FullScreenControl fullScreenControl;

    @Inject
    CacheManager cacheManager;
    @Inject

    CachedAccountInfoStore cachedAccountInfoStore;

    @Inject
    TraficQuickResponseService traficQuickResponseService;

    @Inject
    DrawerHelper drawerHelper;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TraficQuickResponseApplication.getComponent().inject(this);
        if (cachedAccountInfoStore.containsAccountInfo()) {
            startActivity(new Intent(LoginActivity.this, HomeActivity.class));
            sendBroadcast(new Intent("ACTION_START"));
            finish();
        }

        requestPermission();

        LocalBroadcastManager.getInstance(this).registerReceiver(tokenReceiver,
                new IntentFilter("tokenReceiver"));
//        setFullScreen
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);

        connectionDetector = new ConnectionDetector(this);
        initViews();
    }

    BroadcastReceiver tokenReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String token = intent.getStringExtra("token");
            if(token != null)
            {
                Log.d("ctr", "onReceive: ctr " + token);
                cacheManager.saveTokenFCM(token);
                cachedAccountInfoStore.cacheTokenFCMOnly(token);
            }
        }
    };

    private void initViews() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Mohon tunggu...");

        binding.btnLogin.setOnClickListener(
                new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        boolean hasError = false;
                        if (!connectionDetector.isConnectingToInternet()) {
                            Toast.makeText(LoginActivity.this, "Mohon cek koneksi internet", Toast.LENGTH_SHORT).show();
                        } else if (binding.tvNrp.getText().toString().equals("") && binding.tvPassword.getText().toString().equals("")) {
                            Toast.makeText(LoginActivity.this, "NRP dan Password tidak boleh kosong", Toast.LENGTH_SHORT).show();
                            hasError = true;
                        } else if (binding.tvNrp.getText().toString().equals("")) {
                            Toast.makeText(LoginActivity.this, "NRP tidak boleh ksoong", Toast.LENGTH_SHORT).show();
                            hasError = true;
                        } else if (binding.tvPassword.getText().toString().equals("") || binding.tvPassword.getText().toString().isEmpty()) {
                            Toast.makeText(LoginActivity.this, "Password tidak boleh kosong", Toast.LENGTH_SHORT).show();
                            hasError = true;
                        }

                        if (!hasError) {
                            final ProgressDialog progressDialog = new ProgressDialog(LoginActivity.this);
                            progressDialog.setMessage("Please wait...");

                            traficQuickResponseService.login(binding.tvNrp.getText().toString(), binding.tvPassword.getText().toString())
                                    .subscribeOn(Schedulers.io())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe(new Subscriber<UserChannelToken>() {
                                        @Override
                                        public void onStart() {
                                            super.onStart();
                                            progressDialog.show();
                                        }

                                        @Override
                                        public void onCompleted() {
                                            progressDialog.dismiss();
                                        }

                                        @Override
                                        public void onError(Throwable e) {
                                            Log.e("amsibsam", e.toString());
                                            Toast.makeText(LoginActivity.this, "Mohon Cek NRP dan Password", Toast.LENGTH_SHORT).show();
                                            progressDialog.dismiss();
                                        }

                                        @Override
                                        public void onNext(UserChannelToken userChannelToken) {
                                            progressDialog.dismiss();
                                            startService(new Intent(getBaseContext(), PusherNotificationService.class));
                                            Log.d("amsibsam", "channel: " + userChannelToken.getChannel());
                                            Log.d("amsibsam", "token: " + userChannelToken.getAuthenticationToken());
                                            cachedAccountInfoStore.cacheUserToken(userChannelToken);
                                            EventBus.getDefault().post(UserEvent.LOGIN);
                                            startActivity(new Intent(LoginActivity.this, HomeActivity.class));
                                            sendBroadcast(new Intent("ACTION_START"));
                                            finish();
                                        }
                                    });


                        }
                    }
                }

        );


        final int[] count = new int[1];
        binding.tvWelcome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                count[0]++;
                if (count[0] == 4){
                    startActivity(new Intent(LoginActivity.this, SwitchEnvironmentActivity.class));
                }
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        count[0] = 0;
                    }
                }, 4000);
            }
        });
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Checks whether a hardware keyboard is available
        if (newConfig.hardKeyboardHidden == Configuration.HARDKEYBOARDHIDDEN_NO) {
//            keyboard is visible
            Log.d("amsibsam", "show");
            binding.tvWelcome.setVisibility(View.GONE);
        } else if (newConfig.hardKeyboardHidden == Configuration.HARDKEYBOARDHIDDEN_YES) {
//            keyboard is hiden
            Log.d("amsibsam", "hide");
            binding.tvWelcome.setVisibility(View.VISIBLE);
        }
    }


    private void requestPermission(){
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(LoginActivity.this,
                Manifest.permission.READ_CONTACTS)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(LoginActivity.this,
                    Manifest.permission.READ_CONTACTS)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(LoginActivity.this,
                        new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.DISABLE_KEYGUARD, Manifest.permission.INTERNET, Manifest.permission.VIBRATE, Manifest.permission.WAKE_LOCK, Manifest.permission.RECEIVE_BOOT_COMPLETED},
                        1);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
    }
}

