package com.ics.personil.ui.utils;

import android.databinding.BindingAdapter;
import android.net.Uri;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

public class BindingAdapters {

    @BindingAdapter({"picasso"})
    public static void bindImageResource(ImageView imageView, Uri imageUri) {
        Picasso.with(imageView.getContext())
                .load(imageUri)
                .fit()
                .centerCrop()
                .into(imageView);
    }

    @BindingAdapter({"binding"})
    public static void bindImageResource(ImageView imageView, int resourceId) {
        imageView.setImageResource(resourceId);
    }
}
