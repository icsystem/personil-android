package com.ics.personil.ui.activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.ics.personil.TraficQuickResponseApplication;
import com.ics.personil.UserEvent;
import com.ics.personil.R;
import com.ics.personil.UserLocation;
import com.ics.personil.databinding.ActivityHomeBinding;
import com.ics.personil.internal.AppsDataStore;
import com.ics.personil.internal.CachedAccountInfoStore;
import com.ics.personil.internal.LocationService;
import com.ics.personil.internal.SendLocationBroadcastRecevier;
import com.ics.personil.network.AppReceiver;
import com.ics.personil.network.TraficQuickResponseService;
import com.ics.personil.pojo.AccountInfo;
import com.ics.personil.ui.utils.DrawerHelper;
import com.ics.personil.ui.utils.FullScreenControl;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Inject;

import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.SmartLocation;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

public class HomeActivity extends AppCompatActivity implements OnMapReadyCallback, android.location.LocationListener {

    private static final String TAG = HomeActivity.class.getSimpleName();
    @Inject
    AppsDataStore appsDataStore;

    @Inject
    TraficQuickResponseService traficQuickResponseService;

    @Inject
    FullScreenControl fullScreenControl;

    @Inject
    CachedAccountInfoStore cachedAccountInfoStore;

    @Inject
    DrawerHelper drawerHelper;

    public static HomeActivity homeActivity;
    private AlertDialog alert;
    private ActivityHomeBinding binding;
    private ProgressDialog progressDialog;
    private AlarmManager alarmManager;
    private Intent notificationIntent;
    private PendingIntent pendingIntent;
    private GoogleMap map;
    private boolean isLogout = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TraficQuickResponseApplication.getComponent().inject(this);

        fullScreenControl.setFullScreen(this);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_home);
        isLogout = getIntent().getBooleanExtra("logout", false);

        if (isLogout){
            logout();
        }


        drawerHelper.initDrawer(this, 1);
        homeActivity = this;

        notificationIntent = new Intent(this, SendLocationBroadcastRecevier.class);
        pendingIntent = PendingIntent.getBroadcast(this, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        TraficQuickResponseApplication.getSharedPreferencesFirebase().edit().putBoolean("is_login", true).apply();

        isGoogleMapInstalled("com.google.android.apps.maps", this);
        setUpMap();
        initViews();
        getAccountInfo();
//        scheduleSendLocation();
        initLocation();
    }

    @Override
    protected void onResume() {
        super.onResume();

        LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        boolean statusOfGPS = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        Log.d("amsibsam", "gps status " + statusOfGPS);
        if (!statusOfGPS) {
            showGpsOnDialog(this);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        setUpLocation(googleMap);

        if (getIntent().hasExtra("lat")){
            try{
                float latitude = Float.parseFloat(getIntent().getStringExtra("lat"));
                float longitude = Float.parseFloat(getIntent().getStringExtra("lng"));
                focusToMarker(googleMap, "Location", latitude, longitude);
            } catch (Exception e){

            }

        }

    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d("amsibsam", "latlong location change " + location.getLatitude() + ", " + location.getLongitude());
        float lat = 0;
        float lng = 0;
        if (location != null){
            lat = (float) location.getLatitude();
            lng = (float) location.getLongitude();
        }

        EventBus.getDefault().post(new UserLocation(lat, lng));
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }


    private void setUpMap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map_fragment);
        mapFragment.getMapAsync(this);
    }

    private void isGoogleMapInstalled(String packagename, Context context) {
        PackageManager pm = context.getPackageManager();
        try {
            pm.getPackageInfo(packagename, PackageManager.GET_ACTIVITIES);
        } catch (PackageManager.NameNotFoundException e) {
            openGoogleMapOnMarket(this);
        }
    }

    private void openGoogleMapOnMarket(final Activity activity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage(
                "Google map tidak terinstall, silahkan unduh terlebih dahulu")
                .setTitle("Google Map tidak terinstall")
                .setCancelable(false)
                .setPositiveButton("Install",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                String packageName = "com.google.android.apps.maps";

                                try {
                                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + packageName)));
                                } catch (ActivityNotFoundException anfe) {
                                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + packageName)));
                                }
                                alert.dismiss();
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                alert.dismiss();
                            }
                        });
        alert = builder.create();
        alert.show();
    }

    private void focusToMarker(GoogleMap map, String markerTitle, float latitude, float longitude) {
//        TODO : customize marker
        final LatLng location = new LatLng(latitude, longitude);
        map.addMarker(new MarkerOptions().position(location).title(markerTitle));
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(location, 12f));
    }

    private void focusToCurrentLocation(Location currentLocation) {
        final LatLng location = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(location, 12f));
    }

    private void initViews() {

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...");


        binding.statusGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                Log.d("amsibsam", "checkedId" + checkedId);
                RadioButton radioStatus = (RadioButton) findViewById(checkedId);
                Log.d("amsibsam", "text " + radioStatus.getText());
                setUserStatusToServer(radioStatus.getText().toString());
            }
        });

        binding.icsLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerHelper.getDrawer().openDrawer();
            }
        });



    }

    public void logout() {
        Intent service = new Intent(HomeActivity.this, LocationService.class);
        stopService(service);
        stopBroadcastReceiver();
        if (alarmManager != null){
            alarmManager.cancel(pendingIntent);
        }
        TraficQuickResponseApplication.getSharedPreferencesFirebase().edit().putBoolean("is_login", false).apply();
        appsDataStore.logout()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Void>() {
                    @Override
                    public void call(Void aVoid) {
                        Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        progressDialog.dismiss();
                        EventBus.getDefault().post(UserEvent.LOGOUT);
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        Toast.makeText(HomeActivity.this, throwable.getMessage(), Toast.LENGTH_SHORT).show();
                        throwable.printStackTrace();
                        progressDialog.dismiss();
                    }
                });
    }

    private void getAccountInfo() {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("loading..");

        traficQuickResponseService.getAccount()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<AccountInfo>() {
                    @Override
                    public void onStart() {
//                        progressDialog.show();
                    }

                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
//                        progressDialog.dismiss();
                        Log.d("Home Activity", "onError: " + e.getMessage());
//                        Toast.makeText(HomeActivity.this, "error mendapatkan data", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onNext(AccountInfo accountInfo) {
                        Log.d("Home channel ", "onNext: ctr " + cachedAccountInfoStore.getChannel());
//                        progressDialog.dismiss();
                        cachedAccountInfoStore.cahceAccountInfo(accountInfo);
                        binding.tvNama.setText(accountInfo.formatName);
                        binding.tvPangkat.setText(accountInfo.getJobPosition().getTitle());
                        binding.tvNrp.setText(accountInfo.nrpNumber);
                        setUserStatus(accountInfo.onlineStatus);
                    }
                });


    }

    private void showGpsOnDialog(final Activity activity) {

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage(
                "You need to activate location service to use this feature. Please turn on network or GPS mode in location setting")
                .setTitle("GPS disabled")
                .setCancelable(false)
                .setPositiveButton("Turn on",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent intent = new Intent(
                                        Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                activity.startActivity(intent);
                                alert.dismiss();
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                alert.dismiss();
                            }
                        });
        alert = builder.create();
        alert.show();
    }

    private void setUserStatus(String statusUser) {
        if (statusUser.equals("lepas_dinas")) {
            binding.radioLepasDinas.setChecked(true);
            setUpdateLocation(false);
            alarmManager.cancel(pendingIntent);
        } else if (statusUser.equals("cadangan")) {
            binding.radioCadangan.setChecked(true);
            setUpdateLocation(true);
            startRequiredService(10);
        } else if (statusUser.equals("dinas")) {
            binding.radioDinas.setChecked(true);
            setUpdateLocation(true);
            startRequiredService(10);
        }
    }

    private void setUserStatusToServer(String statusUser) {
        String statusToLower = statusUser.toLowerCase().replace(" ", "_");
        Log.d("amsibsam", "status update " + statusToLower);
        traficQuickResponseService.setOnlineStatus(statusToLower)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Void>() {

                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Toast.makeText(HomeActivity.this, "Error silahkan coba lagi", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onNext(Void aVoid) {
                        progressDialog.dismiss();
                        getAccountInfo();
                    }
                });
    }

    private void setUpLocation(GoogleMap googleMap) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        googleMap.setMyLocationEnabled(true);

        // Get LocationManager object from System Service LOCATION_SERVICE
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        // Create a criteria object to retrieve provider
        Criteria criteria = new Criteria();
        // Get the name of the best provider
        String provider = locationManager.getBestProvider(criteria, true);
        // Get Current Location
        Location myLocation = locationManager.getLastKnownLocation(provider);
        double latitude = 0;
        double longitude = 0;
        if (myLocation!=null){
            latitude = myLocation.getLatitude();
            longitude = myLocation.getLongitude();
        }

        EventBus.getDefault().post(new UserLocation((float) latitude, (float) longitude));


        LatLng latLng = new LatLng(-7.202224, 110.194015);

        googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));

        googleMap.animateCamera(CameraUpdateFactory.zoomTo(7));
    }

    private void scheduleSendLocation()
    {
        Intent notificationIntent = new Intent(this, SendLocationBroadcastRecevier.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
        alarmManager.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,0,1000*5,pendingIntent);
    }


    private void startRequiredService(int time)
    {
        PackageManager pm  = HomeActivity.this.getPackageManager();
        ComponentName componentName = new ComponentName(HomeActivity.this, SendLocationBroadcastRecevier.class);
        pm.setComponentEnabledSetting(componentName,PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                PackageManager.DONT_KILL_APP);

        ComponentName componentName2 = new ComponentName(HomeActivity.this, AppReceiver.class);
        pm.setComponentEnabledSetting(componentName2,PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                PackageManager.DONT_KILL_APP);
        alarmManager = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
        alarmManager.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,0,1000*time,pendingIntent);

        Intent mServiceIntent = new Intent(this, LocationService.class);
        startService(mServiceIntent);
    }

    private void stopBroadcastReceiver(){
        PackageManager pm  = HomeActivity.this.getPackageManager();
        ComponentName componentName = new ComponentName(HomeActivity.this, SendLocationBroadcastRecevier.class);
        pm.setComponentEnabledSetting(componentName,PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                PackageManager.DONT_KILL_APP);

        ComponentName componentName2 = new ComponentName(HomeActivity.this, AppReceiver.class);
        pm.setComponentEnabledSetting(componentName2,PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                PackageManager.DONT_KILL_APP);
    }

    private void initLocation(){
        SmartLocation.with(this).location().oneFix().start(new OnLocationUpdatedListener() {
            @Override
            public void onLocationUpdated(Location location) {
                Log.d("smartLocation", ": "+location);
                float lat = (float) location.getLatitude();
                float lng = (float) location.getLongitude();
                traficQuickResponseService.pushLocation(lat, lng);
            }
        });

    }

    private void setUpdateLocation(boolean isUpdating){
        if (isUpdating){
            PackageManager pm  = HomeActivity.this.getPackageManager();
            ComponentName componentName = new ComponentName(HomeActivity.this, SendLocationBroadcastRecevier.class);
            pm.setComponentEnabledSetting(componentName,PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                    PackageManager.DONT_KILL_APP);
        } else {
            PackageManager pm  = HomeActivity.this.getPackageManager();
            ComponentName componentName = new ComponentName(HomeActivity.this, SendLocationBroadcastRecevier.class);
            pm.setComponentEnabledSetting(componentName,PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                    PackageManager.DONT_KILL_APP);
        }
    }

    private void requestPermition(){
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(HomeActivity.this,
                Manifest.permission.READ_CONTACTS)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(HomeActivity.this,
                    Manifest.permission.READ_CONTACTS)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(HomeActivity.this,
                        new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                        1);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
    }
}
