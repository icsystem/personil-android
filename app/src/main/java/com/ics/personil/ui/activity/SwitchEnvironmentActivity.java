package com.ics.personil.ui.activity;

import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.ics.personil.R;
import com.ics.personil.TraficQuickResponseApplication;
import com.ics.personil.databinding.ActivitySwitchEnvironmentBinding;
import com.ics.personil.internal.CacheManager;

import javax.inject.Inject;

public class SwitchEnvironmentActivity extends AppCompatActivity {
    private ActivitySwitchEnvironmentBinding binding;
    private String endpointUrl;
    private static String staging;
    private static String production;

    @Inject
    CacheManager cacheManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TraficQuickResponseApplication.getComponent().inject(this);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_switch_environment);

        endpointUrl = cacheManager.getEndpointUrl();

        staging = getResources().getString(R.string.staging_api);

        binding.rdStaging.setText("Staging (" + getResources().getString(R.string.staging_api) + ")");
        binding.rdProduction.setText("Production (" + getResources().getString(R.string.production_api) + ")");

        if (endpointUrl.equals(staging)){
            binding.rdStaging.setChecked(true);
        } else {
            binding.rdProduction.setChecked(true);
        }

        binding.baseUrlRadio.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton checkedRadioButton = (RadioButton) findViewById(checkedId);
                Log.d("amsibsam", "checked radio button "+checkedRadioButton.getText());
                if (checkedRadioButton.getText().toString().contains("Staging")){
                    cacheManager.setEndpointUrl(getResources().getString(R.string.staging_api));
                } else {
                    cacheManager.setEndpointUrl(getResources().getString(R.string.production_api));
                }
            }
        });
    }
}
