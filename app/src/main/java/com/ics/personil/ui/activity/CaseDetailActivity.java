package com.ics.personil.ui.activity;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.ics.personil.R;
import com.ics.personil.TraficQuickResponseApplication;
import com.ics.personil.databinding.ActivityCaseDetailBinding;
import com.ics.personil.network.TraficQuickResponseService;
import com.ics.personil.ui.dialog.CheckInDilaog;


import javax.inject.Inject;

import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;


public class CaseDetailActivity extends AppCompatActivity implements OnMapReadyCallback, android.location.LocationListener,
        CheckInDilaog.OnClickListener {
    private static final String TAG = CaseDetailActivity.class.getSimpleName();

    private final static String EVENT_TYPE = "event_type";
    private final static String DESCRIPTION_KEY = "message";
    private final static String LOCATION_KEY = "location";
    private final static String LAT_KEY = "lat";
    private final static String LNG_KEY = "lng";
    private final static String CASE_ID = "case_id";

    private Circle circle;
    private ActivityCaseDetailBinding binding;
    private AlertDialog alert;
    private Marker marker;

    @Inject
    TraficQuickResponseService traficQuickResponseService;
    private boolean statusDistance;

    public static Intent generateCaseDetailActivity(Context context, String userEvent, String descprition, double lat, double lng, String location, String caseId) {
        Intent intent = new Intent(context, CaseDetailActivity.class);
        intent.putExtra(EVENT_TYPE, userEvent);
        intent.putExtra(DESCRIPTION_KEY, descprition);
        intent.putExtra(LOCATION_KEY, location);
        intent.putExtra(LAT_KEY, lat);
        intent.putExtra(LNG_KEY, lng);
        intent.putExtra(CASE_ID, caseId);

        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TraficQuickResponseApplication.getComponent().inject(this);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_case_detail);

        isGoogleMapInstalled("com.google.android.apps.maps", this);
        setUpMap();
        initView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        boolean statusOfGPS = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        Log.d("ctr", "gps status " + statusOfGPS);
        if (!statusOfGPS) {
            showGpsOnDialog(this);
        }
    }

    private void isGoogleMapInstalled(String packagename, Context context) {
        PackageManager pm = context.getPackageManager();
        try {
            pm.getPackageInfo(packagename, PackageManager.GET_ACTIVITIES);
        } catch (PackageManager.NameNotFoundException e) {
            openGoogleMapOnMarket(this);
        }
    }

    private void setUpMap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map_fragment);
        mapFragment.getMapAsync(this);
    }

    private void showGpsOnDialog(final Activity activity) {

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage(
                "You need to activate location service to use this feature. Please turn on network or GPS mode in location setting")
                .setTitle("GPS disabled")
                .setCancelable(false)
                .setPositiveButton("Turn on",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent intent = new Intent(
                                        Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                activity.startActivity(intent);
                                alert.dismiss();
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                alert.dismiss();
                            }
                        });
        alert = builder.create();
        alert.show();
    }

    private void openGoogleMapOnMarket(final Activity activity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage(
                "Google map tidak terinstall, silahkan unduh terlebih dahulu")
                .setTitle("Google Map tidak terinstall")
                .setCancelable(false)
                .setPositiveButton("Install",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                String packageName = "com.google.android.apps.maps";

                                try {
                                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + packageName)));
                                } catch (ActivityNotFoundException anfe) {
                                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + packageName)));
                                }
                                alert.dismiss();
                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                alert.dismiss();
                            }
                        });
        alert = builder.create();
        alert.show();
    }

    private void initView() {
        String userEvent = getIntent().getStringExtra(EVENT_TYPE);
        String description = getIntent().getStringExtra(DESCRIPTION_KEY);
        String location = getIntent().getStringExtra(LOCATION_KEY);
        final String caseId = getIntent().getStringExtra(CASE_ID);

        binding.tvTitle.setText(userEvent);
        binding.tvLocation.setText(location);
        binding.tvDescription.setText(description);

        binding.btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(statusDistance){
                    CheckInDilaog.newInstance(CaseDetailActivity.this, caseId).show(getSupportFragmentManager(), TAG);
                }else{
                    Toast.makeText(CaseDetailActivity.this, "Tidak bisa Check In karena di luar radius kejadian", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        double lat = getIntent().getDoubleExtra(LAT_KEY,0);
        double lang = getIntent().getDoubleExtra(LNG_KEY,0);
        Log.d("ctr", "onMapReady: ctr " + lat + " long " + lang);
        final LatLng location = new LatLng(lat, lang);
        googleMap.addMarker(new MarkerOptions().position(location).title("location"));
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(location, 12f));
        drawMarkerWithCircle(location,googleMap,100);

        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        // Showing / hiding your current location
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        googleMap.setMyLocationEnabled(true);

        // Enable / Disable zooming controls
        googleMap.getUiSettings().setZoomControlsEnabled(true);

        // Enable / Disable my location button
        googleMap.getUiSettings().setMyLocationButtonEnabled(true);

        // Enable / Disable Compass icon
        googleMap.getUiSettings().setCompassEnabled(true);

        // Enable / Disable Rotate gesture
        googleMap.getUiSettings().setRotateGesturesEnabled(true);

        // Enable / Disable zooming functionality
        googleMap.getUiSettings().setZoomGesturesEnabled(true);

        googleMap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
            @Override
            public void onMyLocationChange(Location location) {
                float[] distance = new float[2];
                Log.d(TAG, "onMyLocationChange: ctr " + location.getLatitude());
                        /*
                        Location.distanceBetween( mMarker.getPosition().latitude, mMarker.getPosition().longitude,
                                mCircle.getCenter().latitude, mCircle.getCenter().longitude, distance);
                                */

                Location.distanceBetween( location.getLatitude(), location.getLongitude(),
                        circle.getCenter().latitude, circle.getCenter().longitude, distance);

                if( distance[0] > circle.getRadius()  ){
                    statusDistance = false;
//                    Toast.makeText(getBaseContext(), "Diluar, jarak dari kejadian: " + distance[0] + " radius: " + circle.getRadius(), Toast.LENGTH_LONG).show();
                } else {
                    statusDistance = true;
//                    Toast.makeText(getBaseContext(), "Didalam, jarak dari kejadian: " + distance[0] + " radius: " + circle.getRadius() , Toast.LENGTH_LONG).show();
                }

            }
        });
    }

    private void drawMarkerWithCircle(LatLng position,GoogleMap googleMap,double radiusInMeters){

        int strokeColor = 0xffff0000; //red outline
        int shadeColor = 0x44ff0000; //opaque red fill

        CircleOptions circleOptions = new CircleOptions().center(position).radius(radiusInMeters).fillColor(shadeColor).strokeColor(strokeColor).strokeWidth(8);
        circle = googleMap.addCircle(circleOptions);
        Log.d(TAG, "drawMarkerWithCircle: crcle " + circle.getRadius());

        MarkerOptions markerOptions = new MarkerOptions().position(position);
        marker = googleMap.addMarker(markerOptions);
    }

    @Override
    public void onSubmit(final String description, String caseId, final DialogFragment dialogFragment) {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.show();

        traficQuickResponseService.checkIn(caseId,description)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Void>() {
                    @Override
                    public void call(Void aVoid) {
                        progressDialog.dismiss();
                        dialogFragment.dismiss();
                        Toast.makeText(CaseDetailActivity.this, "Sukses Check In", Toast.LENGTH_SHORT).show();
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        progressDialog.dismiss();
                        Log.d(TAG, "call: " + throwable.toString());
                        throwable.printStackTrace();
                        if("retrofit.HttpException: HTTP 401 Unauthorized".contains(throwable.toString())){
                            Toast.makeText(CaseDetailActivity.this, "Sudah pernah Check In", Toast.LENGTH_SHORT).show();
                        }

                    }
                });
    }
}
