package com.ics.personil.ui.activity;

import android.app.KeyguardManager;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.PowerManager;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.ics.personil.R;
import com.ics.personil.TraficQuickResponseApplication;
import com.ics.personil.databinding.ActivityNotificationBinding;
import com.ics.personil.network.PusherService;
import com.ics.personil.network.TraficQuickResponseService;
import com.ics.personil.ui.utils.FullScreenControl;

import javax.inject.Inject;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class NotificationActivity extends AppCompatActivity {
    private ActivityNotificationBinding binding;

    private final static String ACTION_KEY = "action";
    private final static String MESSAGE_KEY = "message";
    private final static String LOCATION_KEY = "location";
    private final static String LAT_KEY = "lat";
    private final static String LNG_KEY = "lng";
    private final static String CASE_ID = "case_id";
    private final static String DESCRIPTION = "description";

    private MediaPlayer soundAlert;
    private Vibrator vibrator;
    private PowerManager.WakeLock fullWakeLock;
    private PowerManager.WakeLock partialWakeLock;
    private int musicVolume;
    private AudioManager audioManager;

    @Inject
    FullScreenControl fullScreenControl;

    @Inject
    TraficQuickResponseService traficQuickResponseService;

    public static Intent generateNotificationActivity(Context context, String userEvent, String messagePushnotif, String lat, String lng, String location, String caseId) {
        Intent intent = new Intent(context, NotificationActivity.class);
        intent.putExtra(ACTION_KEY, userEvent);
        intent.putExtra(MESSAGE_KEY, messagePushnotif);
        intent.putExtra(LOCATION_KEY, location);
        intent.putExtra(LAT_KEY, lat);
        intent.putExtra(LNG_KEY, lng);
        intent.putExtra(CASE_ID, caseId);
        intent.putExtra(DESCRIPTION, messagePushnotif);

        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        createWakeLocks();
        TraficQuickResponseApplication.getComponent().inject(this);
        fullScreenControl.setFullScreen(this);


        binding = DataBindingUtil.setContentView(this, R.layout.activity_notification);

        if (HomeActivity.homeActivity != null){
            HomeActivity.homeActivity.finish();
        }

        readCase();

        String userEvent = getIntent().getStringExtra(ACTION_KEY).toUpperCase();
        String messagePushnotif = getIntent().getStringExtra(MESSAGE_KEY);
        final String latitude = getIntent().getStringExtra(LAT_KEY);
        final String longitude = getIntent().getStringExtra(LNG_KEY);
        String location = getIntent().getStringExtra(LOCATION_KEY);

        if (userEvent.equals(PusherService.ChannelEvent.KEMACETAN.name().toUpperCase())) {
            binding.tvTitle.setText("KEMACETAN");
            binding.tvTitle.setTextColor(getResources().getColor(R.color.black));
            binding.background.setImageResource(R.drawable.bg_kemacetan);
            binding.logoAction.setImageResource(R.drawable.logo_kemacetan);
            binding.divider.setBackgroundColor(getResources().getColor(R.color.yellow_kemacetan));
            binding.imgWarning.setImageResource(R.drawable.warning);

            binding.tvMessage.setText(messagePushnotif + "\nlokasi : "+location);
        } else if (userEvent.equals(PusherService.ChannelEvent.APEL_PERS.name().toUpperCase())) {
            binding.tvTitle.setText("APEL PERS");
            binding.tvTitle.setTextColor(getResources().getColor(R.color.white));
            binding.background.setImageResource(R.drawable.bg_penggelaran);
            binding.logoAction.setImageResource(R.drawable.logo_penggelaran);
            binding.divider.setBackgroundColor(getResources().getColor(R.color.green_pers));
            binding.imgWarning.setImageResource(R.drawable.warning);

            binding.tvMessage.setText(messagePushnotif + "\n dengan lokasi : "+location);
        } else if (userEvent.equals(PusherService.ChannelEvent.KECELAKAAN.name().toUpperCase())) {
            binding.tvTitle.setText("KECELAKAAN");
            binding.tvTitle.setTextColor(getResources().getColor(R.color.white));
            binding.background.setImageResource(R.drawable.bg_kecelakaan);
            binding.logoAction.setImageResource(R.drawable.logo_kecelakaan);
            binding.divider.setBackgroundColor(getResources().getColor(R.color.red_kecelakaan));
            binding.imgWarning.setImageResource(R.drawable.warning_kecelakaan);

            binding.tvMessage.setText(messagePushnotif + "\n dengan lokasi : "+location);
        } else if (userEvent.equals(PusherService.ChannelEvent.PLB.name().toUpperCase())) {
            binding.tvTitle.setText("PLB");
            binding.tvTitle.setTextColor(getResources().getColor(R.color.white));
            binding.background.setImageResource(R.drawable.bg_plb);
            binding.logoAction.setImageResource(R.drawable.logo_plb);
            binding.divider.setBackgroundColor(getResources().getColor(R.color.blue_plb));
            binding.imgWarning.setImageResource(R.drawable.warning);

            binding.tvMessage.setText(messagePushnotif + "\nlokasi : "+location);
        } else if (userEvent.equals(PusherService.ChannelEvent.BANTUAN_MEDIS.name().toUpperCase())){
            binding.tvTitle.setText("Bantuan Medis");
            binding.tvTitle.setTextColor(getResources().getColor(R.color.white));
            binding.background.setImageResource(R.drawable.background);
            binding.logoAction.setImageResource(R.drawable.btn_sos_health);
            binding.divider.setBackgroundColor(getResources().getColor(R.color.blue_plb));
            binding.imgWarning.setImageResource(R.drawable.warning);

            binding.tvMessage.setText(messagePushnotif + "\nlokasi : "+location);
        } else if (userEvent.equals(PusherService.ChannelEvent.BENCANA_KEBAKARAN.name().toUpperCase())){
            binding.tvTitle.setText("Bencana Kebakaran");
            binding.tvTitle.setTextColor(getResources().getColor(R.color.white));
            binding.background.setImageResource(R.drawable.background);
            binding.logoAction.setImageResource(R.drawable.btn_sos_fire);
            binding.divider.setBackgroundColor(getResources().getColor(R.color.red_kecelakaan));
            binding.imgWarning.setImageResource(R.drawable.warning);

            binding.tvMessage.setText(messagePushnotif + "\nlokasi : "+location);
        } else if (userEvent.equals(PusherService.ChannelEvent.KEJAHATAN.name().toUpperCase())){
            binding.tvTitle.setText("Kejahatan");
            binding.tvTitle.setTextColor(getResources().getColor(R.color.white));
            binding.background.setImageResource(R.drawable.background);
            binding.logoAction.setImageResource(R.drawable.btn_sos_crime);
            binding.divider.setBackgroundColor(getResources().getColor(R.color.yellow_kemacetan));
            binding.imgWarning.setImageResource(R.drawable.warning);

            binding.tvMessage.setText(messagePushnotif + "\nlokasi : "+location);
        } else {
            binding.tvTitle.setText(userEvent);
            binding.tvTitle.setTextColor(getResources().getColor(R.color.white));
            binding.background.setImageResource(R.drawable.bg_plb);
            binding.logoAction.setImageResource(R.drawable.logo_plb);
            binding.divider.setBackgroundColor(getResources().getColor(R.color.blue_plb));
            binding.imgWarning.setImageResource(R.drawable.warning);

            binding.tvMessage.setText(messagePushnotif + "\nlokasi : "+location);
        }

        binding.btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(NotificationActivity.this, HomeActivity.class)
                        .putExtra("lat", latitude)
                        .putExtra("lng", longitude));
                finish();
            }
        });

        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        playAlert();


    }

    @Override
    protected void onResume() {
        super.onResume();
        wakeDevice();
    }

    @Override
    protected void onPause() {
        super.onPause();
        fullWakeLock.release();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (fullWakeLock.isHeld()){
            fullWakeLock.release();
        }
        stopAlert();
    }

    @Override
    public void onBackPressed() {
        return;
    }

    private void playAlert() {
        Log.d("amsibsam", "play alert");
        soundAlert = MediaPlayer.create(this, R.raw.sirine);
        soundAlert.setLooping(true);

        vibrator = ((Vibrator) getSystemService(Context.VIBRATOR_SERVICE));
        soundAlert.setAudioStreamType(AudioManager.STREAM_MUSIC);
        soundAlert.start();

        switch (audioManager.getRingerMode()) {
            case AudioManager.RINGER_MODE_NORMAL:
                int ringVolume = audioManager.getStreamVolume(AudioManager.STREAM_RING);
                int maxRingVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_RING);
                musicVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
                int maxMusicVolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
                final int calculatedVolume = ringVolume * maxMusicVolume / maxRingVolume;
                Log.d("ctr", "playAlert: ctr " + "vol " +  musicVolume +" ring " + ringVolume + " max " + maxRingVolume);
                audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, maxMusicVolume, AudioManager.FLAG_PLAY_SOUND);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        try {

                        } catch (IllegalStateException e) {
                            e.printStackTrace();
                        }
                    }
                }, 800);

                vibrator.vibrate(new long[]{500, 500}, 0);
                break;
            case AudioManager.RINGER_MODE_VIBRATE:
                vibrator.vibrate(new long[]{500, 500}, 0);
        }
    }

    private void stopAlert(){
        soundAlert.stop();
        soundAlert.release();
        vibrator.cancel();
    }

    public void wakeDevice() {
        fullWakeLock.acquire();

        KeyguardManager keyguardManager = (KeyguardManager) getSystemService(Context.KEYGUARD_SERVICE);
        KeyguardManager.KeyguardLock keyguardLock = keyguardManager.newKeyguardLock("TAG");
        keyguardLock.disableKeyguard();
    }

    protected void createWakeLocks(){
        PowerManager powerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
        fullWakeLock = powerManager.newWakeLock((PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP), "FULL WAKE LOCK");
        partialWakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "PARTIAL WAKE LOCK");
    }

    private void readCase(){
        String caseId = getIntent().getStringExtra(CASE_ID);
        traficQuickResponseService.readCase(caseId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<Void>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.getMessage();
                        readCase();
                    }

                    @Override
                    public void onNext(Void aVoid) {

                    }
                });
    }
}
