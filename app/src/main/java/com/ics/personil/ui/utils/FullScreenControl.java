package com.ics.personil.ui.utils;

import android.app.Activity;
import android.view.Window;
import android.view.WindowManager;

/**
 * Created by rahardyan on 26/08/16.
 */

public class FullScreenControl {
    public FullScreenControl() {
    }

    public void setFullScreen(Activity activity){
        activity.requestWindowFeature(Window.FEATURE_NO_TITLE);
        activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }
}
