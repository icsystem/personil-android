package com.ics.personil.ui.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.ics.personil.R;
import com.ics.personil.TraficQuickResponseApplication;
import com.ics.personil.databinding.ActivityOpenCaseBinding;
import com.ics.personil.databinding.ItemOpenCaseBinding;
import com.ics.personil.network.TraficQuickResponseService;
import com.ics.personil.pojo.OpenCase;
import com.ics.personil.ui.utils.DrawerHelper;
import com.ics.personil.ui.utils.ListAdapter;
import com.trello.rxlifecycle.components.support.RxAppCompatActivity;

import java.util.List;

import javax.inject.Inject;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

public class OpenCaseActivity extends RxAppCompatActivity {

    private static final String TAG = OpenCaseActivity.class.getSimpleName();

    private ActivityOpenCaseBinding binding;
    private ListAdapter<OpenCase> adapter;

    @Inject
    TraficQuickResponseService traficQuickResponseService;

    @Inject
    DrawerHelper drawerHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TraficQuickResponseApplication.getComponent().inject(this);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_open_case);
        drawerHelper.initDrawer(this, 2);
        adapter = createAdapterFactory();
        binding.listOpencase.setAdapter(adapter);

        binding.icsLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerHelper.getDrawer().openDrawer();
            }
        });
    }

    private void loadOpenCases() {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Mohon Tunggu...");

        traficQuickResponseService.getOpenCases()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(this.<List<OpenCase>>bindToLifecycle())
                .flatMap(new Func1<List<OpenCase>, Observable<OpenCase>>() {
                    @Override
                    public Observable<OpenCase> call(List<OpenCase> openCases) {
                        if(openCases.isEmpty()){
                            binding.textMessage.setVisibility(View.VISIBLE);
                            binding.listOpencase.setVisibility(View.GONE);
                        }else{
                            binding.textMessage.setVisibility(View.GONE);
                            binding.listOpencase.setVisibility(View.VISIBLE);
                        }
                        return Observable.from(openCases);
                    }
                })
                .subscribe(new Subscriber<OpenCase>() {
                    @Override
                    public void onStart() {
                        super.onStart();
                        progressDialog.show();
                    }

                    @Override
                    public void onCompleted() {
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        progressDialog.dismiss();
                        e.printStackTrace();
                        Toast.makeText(OpenCaseActivity.this, "Unable to load cases!", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onNext(OpenCase openCase) {
                        adapter.addOrUpdate(openCase);
                    }
                });
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadOpenCases();
    }

    private ListAdapter<OpenCase> createAdapterFactory() {
        return new ListAdapter<OpenCase>(this) {
            @Override
            protected View generateView(int position, ViewGroup parent) {
                ItemOpenCaseBinding binding = ItemOpenCaseBinding.inflate(layoutInflater, parent, false);
                return binding.getRoot();
            }

            @Override
            public void fillValues(final int position, View convertView) {
                ItemOpenCaseBinding binding = DataBindingUtil.findBinding(convertView);
                binding.textContent.setText(data.get(position).getDescription());
                binding.textLocation.setText(data.get(position).getLocation());
                binding.textCase.setText(data.get(position).getCaseType().getTitle());

                binding.layoutItem.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startActivity(new Intent(CaseDetailActivity.generateCaseDetailActivity(OpenCaseActivity.this,
                                data.get(position).getCaseType().getEventName(),
                                data.get(position).getDescription(),
                                data.get(position).getLat(),
                                data.get(position).getLng(),
                                data.get(position).getLocation(),
                                data.get(position).getId())));
                    }
                });
            }
        };
    }
}
