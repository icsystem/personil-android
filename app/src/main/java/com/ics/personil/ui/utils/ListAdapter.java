package com.ics.personil.ui.utils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by angga on 9/10/16.
 */
public abstract class ListAdapter<Item> extends BaseAdapter {
    private static final String TAG = ListAdapter.class.getSimpleName();

    protected Context context;
    protected LayoutInflater layoutInflater;
    protected List<Item> data;

    public ListAdapter(Context context) {
        this.context = context;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        data = new ArrayList<>();
    }

    @Override
    public int getCount() {
        try {
            return data.size();
        } catch (Exception e) {
            return 0;
        }
    }

    @Override
    public Item getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View itemView, ViewGroup parent) {
        ViewHolder holder;
        if (itemView == null) {
            itemView = generateView(position, parent);
            holder = new ViewHolder(itemView);
            itemView.setTag(holder);
        } else {
            holder = (ViewHolder) itemView.getTag();
        }

        fillValues(position, holder.itemView);

        return itemView;
    }

    protected abstract View generateView(int position, ViewGroup parent);

    public abstract void fillValues(final int position, final View convertView);

    protected class ViewHolder {
        View itemView;

        public ViewHolder(View itemView) {
            this.itemView = itemView;
        }
    }

    public List<Item> getData() {
        return data;
    }

    public void add(Item item) {
        data.add(item);
        notifyDataSetChanged();
    }

    public void addIfNotExist(Item item) {
        if (!data.contains(item)) {
            data.add(item);
            notifyDataSetChanged();
        }
    }

    public void addTop(Item item) {
        data.add(0, item);
        notifyDataSetChanged();
    }


    public void addOrUpdate(Item item) {
        int i = data.indexOf(item);
        if (i >= 0) {
            data.set(i, item);
        } else {
            add(item);
        }
        notifyDataSetChanged();
    }

    public void addTopOrUpdate(Item item) {
        int i = data.indexOf(item);
        if (i >= 0) {
            data.set(i, item);
        } else {
            data.add(0, item);
        }
        notifyDataSetChanged();
    }



    public void remove(int position) {
        if (position >= 0 && position < data.size()) {
            data.remove(position);
            notifyDataSetChanged();
        }
    }

    public void remove(Item item) {
        int position = data.indexOf(item);
        remove(position);
    }

    public void clear() {
        data.clear();
        notifyDataSetChanged();
    }
}
