package com.ics.personil.module;

import android.content.Context;

import com.ics.personil.internal.CacheManager;
import com.ics.personil.internal.CachedAccountInfoStore;
import com.ics.personil.network.PusherService;
import com.ics.personil.network.TraficQuickResponseService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by rahardyan on 25/08/16.
 */
@Module
public final class ExternalModule {
    @Provides
    @Singleton
    TraficQuickResponseService provideTraficQuickResponseService(Context context, CachedAccountInfoStore cachedAccountInfoStore,
                                                                 CacheManager cacheManager){
        return new TraficQuickResponseService(context,cachedAccountInfoStore,cacheManager);
    }

    @Provides
    @Singleton
    PusherService providePusherService(){
        return new PusherService();
    }
}
