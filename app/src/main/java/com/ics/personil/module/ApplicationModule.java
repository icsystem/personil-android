package com.ics.personil.module;

import android.app.Application;
import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by rahardyan on 25/08/16.
 */
@Module
public final class ApplicationModule {
    private final Application application;


    public ApplicationModule(Application application) {
        this.application = application;
    }

    @Provides
    @Singleton
    Application provideApplication(){
        return application;
    }

    @Provides
    @Singleton
    Context provideContext(){
        return application;
    }
}
