package com.ics.personil.module;

import android.content.Context;

import com.ics.personil.internal.AppsDataStore;
import com.ics.personil.internal.CacheManager;
import com.ics.personil.internal.CachedAccountInfoStore;
import com.ics.personil.network.TraficQuickResponseService;
import com.ics.personil.ui.utils.DrawerHelper;
import com.ics.personil.ui.utils.FullScreenControl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by rahardyan on 25/08/16.
 */
@Module
public final class InternalModule {
    @Provides
    @Singleton
    FullScreenControl provideFullScreenControll(){
        return new FullScreenControl();
    }

    @Provides
    @Singleton
    CacheManager provideCacheManager(Context context){
        return new CacheManager(context);
    }

    @Provides
    @Singleton
    CachedAccountInfoStore provideCachedAccountInfo(Context context){
        return new CachedAccountInfoStore(context);
    }

    @Provides
    @Singleton
    AppsDataStore provideAppsDataStore(TraficQuickResponseService traficQuickResponseService,
                                       CachedAccountInfoStore cachedAccountInfoStore,
                                       CacheManager cacheManager){
        return new AppsDataStore(traficQuickResponseService,cachedAccountInfoStore,cacheManager);
    }

    @Provides
    @Singleton
    DrawerHelper provideDrawerHelper() {
        return new DrawerHelper();
    }
}
