package com.ics.personil.external;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.ics.personil.R;
import com.ics.personil.TraficQuickResponseApplication;
import com.ics.personil.internal.CacheManager;
import com.ics.personil.ui.activity.NotificationActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by rahardyan on 12/09/16.
 */
public class NotificationsFirebaseMessageingService extends FirebaseMessagingService {
    private static final String TAG = "FCM Service";
    private CacheManager cacheManager;


    public NotificationsFirebaseMessageingService() {

    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("firebase", "listen to firebase");
        FirebaseMessaging.getInstance().subscribeToTopic("global_setting");
        cacheManager = TraficQuickResponseApplication.getCacheManager();
    }


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // TODO: Handle FCM messages here.

        Log.d(TAG, "onMessageReceived: 1 " + remoteMessage.getTo());
        Log.d(TAG, "onMessageReceived: 2 " + remoteMessage.getCollapseKey());
        Log.d(TAG, "onMessageReceived: 3 " + remoteMessage.getSentTime());
        Log.d(TAG, "onMessageReceived: 4 " + remoteMessage.getTtl());
        Log.d(TAG, "onMessageReceived: 5 " + remoteMessage.getData());
        Log.d(TAG, "onMessageReceived: 6 " + remoteMessage.getMessageId());
        Log.d(TAG, "raw data "+remoteMessage.toString());


        // If the application is in the foreground handle both data and notification messages here.
        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated.
        Log.d(TAG, "From: " + remoteMessage.getFrom());
        Log.d(TAG, "Notification Message Body: " + remoteMessage.getData().toString());

        if (remoteMessage.getData().size() > 0 && TraficQuickResponseApplication.getSharedPreferencesFirebase().getBoolean("is_login", false)) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());

            if (remoteMessage.getCollapseKey().equals("setting")){

            } else {
                handleParsingMessage(remoteMessage.getData().toString(), remoteMessage.getCollapseKey());
            }
        } else {
            return;
        }

//        if (remoteMessage.getData().get("message") != null) {
//            sendNotification(remoteMessage.getData().get("message").toString());
//        }

    }

    @Override
    public void onDeletedMessages() {
        Log.d(TAG, "deleted notifications");
    }

    public Bitmap getBitmapFromURL(String strURL) {
        try {
            URL url = new URL(strURL);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private void handleParsingMessage(String data, String eventName) {

        try {
            JSONObject jsonObject = new JSONObject(data);
            JSONObject openCase = jsonObject.getJSONObject("open_case");
            String description = openCase.getString("description");
            String location = openCase.getString("location");
            String latitude = openCase.getString("lat");
            String longitude = openCase.getString("lng");
            String id = openCase.getString("id");

            Intent notificationActivity = NotificationActivity.generateNotificationActivity(getApplication(), eventName, description, latitude, longitude, location, id);
            notificationActivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            notificationActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(notificationActivity);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void sendNotification(String json) {
        Context context = getBaseContext();
        if (json != null && !json.equals("")) {


            Intent intent = new Intent(this, NotificationActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                    PendingIntent.FLAG_UPDATE_CURRENT);


            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                    .setTicker("")
                    .setLargeIcon(BitmapFactory.decodeResource(getApplicationContext().getResources(), R.mipmap.ic_launcher))
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle("")
                    .setContentText("")
                    .setPriority(10)
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent);

            NotificationCompat.InboxStyle notification = new NotificationCompat.InboxStyle(notificationBuilder);
            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            notificationManager.notify(0 /* ID of notification */, notification.build());
        }
    }




}
