package com.ics.personil.external;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.ics.personil.TraficQuickResponseApplication;
import com.ics.personil.internal.CacheManager;


/**
 * Created by rahardyan on 12/09/16.
 */
public class FirebaseIDService extends FirebaseInstanceIdService {
    private static final String TAG = "FirebaseIDService";
    private CacheManager cacheManager;

    @Override
    public void onTokenRefresh() {
        cacheManager = TraficQuickResponseApplication.getCacheManager();
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();

        cacheManager.saveTokenFCM(refreshedToken);
        Log.d(TAG, "Refreshed token: " + refreshedToken);

        // TODO: Implement this method to send any registration to your app's servers.
        sendRegistrationToServer(refreshedToken);
    }

    private void sendRegistrationToServer(String refreshedToken) {
        final Intent intent = new Intent("tokenReceiver");

        final LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(this);
        intent.putExtra("token",refreshedToken);
        broadcastManager.sendBroadcast(intent);
    }
}
