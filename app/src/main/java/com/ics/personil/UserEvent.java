package com.ics.personil;

/**
 * Created by rahardyan on 26/08/16.
 */
public enum UserEvent {
    LOGIN,
    LOGOUT
}
