package com.ics.personil.pojo;

/**
 * Created by angga on 8/29/16.
 */
public class AccountInfo {
    public String id;
    public String name;
    public String formatName;
    public String nrpNumber;
    public String phoneNumber;
    public String channel;
    public String onlineStatus;
    public String onlineStatusString;
    public JobPosition jobPosition;
    public Setting setting;

    public AccountInfo(String id, String name, String formatName, String nrpNumber, String phoneNumber,
                       String channel, String onlineStatus, String onlineStatusString,String jobId,
                       String jobTitle, int updateInterval, int radius, boolean isMonitoring) {
        this.id = id;
        this.name = name;
        this.formatName = formatName;
        this.nrpNumber = nrpNumber;
        this.phoneNumber = phoneNumber;
        this.channel = channel;
        this.onlineStatus = onlineStatus;
        this.onlineStatusString = onlineStatusString;
        this.jobPosition.id = jobId;
        this.jobPosition.title = jobTitle;
        this.setting.updateInterval = updateInterval;
        this.setting.radius = radius;
        this.setting.isMonitoring = isMonitoring;

    }

    public class JobPosition{

        private String id;
        private String title;

        public void setId(String id) {
            this.id = id;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getId() {
            return id;
        }

        public String getTitle() {
            return title;
        }
    }

    public class Setting {
        private int updateInterval;
        private int radius;
        private boolean isMonitoring;

        public void setUpdateInterval(int updateInterval) {
            this.updateInterval = updateInterval;
        }

        public void setRadius(int radius) {
            this.radius = radius;
        }

        public void setMonitoring(boolean monitoring) {
            isMonitoring = monitoring;
        }

        public int getUpdateInterval() {
            return updateInterval;
        }

        public int getRadius() {
            return radius;
        }

        public boolean isMonitoring() {
            return isMonitoring;
        }
    }

    public JobPosition getJobPosition() {
        return jobPosition;
    }

    @Override
    public String toString() {
        return "AccountInfo{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", formatName='" + formatName + '\'' +
                ", nrpNumber='" + nrpNumber + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", channel='" + channel + '\'' +
                ", onlineStatus='" + onlineStatus + '\'' +
                ", onlineStatusString='" + onlineStatusString + '\'' +
                ", jobPosition=" + jobPosition +
                '}';
    }
}
