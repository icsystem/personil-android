package com.ics.personil.pojo;

/**
 * Created by angga on 9/4/16.
 */
public class UserChannelToken {
    private String authenticationToken;
    private String channel;
    private String iidToken;

    public UserChannelToken(String authenticationToken, String channel, String iidToken) {
        this.authenticationToken = authenticationToken;
        this.channel = channel;
        this.iidToken = iidToken;
    }

    public String getAuthenticationToken() {
        return authenticationToken;
    }

    public String getChannel() {
        return channel;
    }

    public String getIidToken() {
        return iidToken;
    }
}
