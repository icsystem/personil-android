package com.ics.personil.pojo;

import java.util.Date;

/**
 * Created by angga on 9/17/16.
 */
public class QuickResponse {

    private String id;
    private Date date;
    private boolean isCheckIn;
    private boolean isReaded;

    public String getId() {
        return id;
    }

    public Date getDate() {
        return date;
    }

    public boolean isCheckIn() {
        return isCheckIn;
    }

    public boolean isReaded() {
        return isReaded;
    }
}
