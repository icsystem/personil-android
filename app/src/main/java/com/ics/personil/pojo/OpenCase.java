package com.ics.personil.pojo;

/**
 * Created by angga on 9/10/16.
 */

public class OpenCase {
    private String id;
    private String location;
    private String description;
    private double lat;
    private double lng;
    private CaseType caseType;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLocation() {
        return location;
    }

    public String getDescription() {
        return description;
    }

    public double getLat() {
        return lat;
    }

    public double getLng() {
        return lng;
    }

    public CaseType getCaseType() {
        return caseType;
    }

    public class CaseType{
        String eventName;
        String title;

        public String getEventName() {
            return eventName;
        }

        public String getTitle() {
            return title;
        }
    }

    @Override
    public String toString() {
        return "OpenCase{" +
                "id='" + id + '\'' +
                ", location='" + location + '\'' +
                ", description='" + description + '\'' +
                ", lat='" + lat + '\'' +
                ", lng='" + lng + '\'' +
                ", caseType=" + caseType +
                '}';
    }
}
