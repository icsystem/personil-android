package com.ics.personil;

import android.app.Application;

import com.ics.personil.internal.SendLocationBroadcastRecevier;
import com.ics.personil.module.ApplicationModule;
import com.ics.personil.module.ExternalModule;
import com.ics.personil.module.InternalModule;
import com.ics.personil.network.PusherNotificationService;
import com.ics.personil.ui.activity.CaseDetailActivity;
import com.ics.personil.ui.activity.HomeActivity;
import com.ics.personil.ui.activity.LoginActivity;
import com.ics.personil.ui.activity.NotificationActivity;
import com.ics.personil.ui.activity.OpenCaseActivity;
import com.ics.personil.ui.activity.SwitchEnvironmentActivity;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by rahardyan on 25/08/16.
 */
@Singleton
@Component(
        modules = {
                ApplicationModule.class,
                InternalModule.class,
                ExternalModule.class
        }
)
public interface TraficQuickResponseComponent {
    void inject(LoginActivity loginActivity);
    void inject(PusherNotificationService pusherNotificationService);
    void inject(NotificationActivity notificationActivity);
    void inject(HomeActivity homeActivity);
    void inject(SendLocationBroadcastRecevier sendLocationBroadcastRecevier);
    void inject(OpenCaseActivity openCaseActivity);
    void inject(CaseDetailActivity caseDetailActivity);
    void inject(SwitchEnvironmentActivity switchEnvironmentActivity);
    void inject(TraficQuickResponseApplication traficQuickResponseApplication);


    final class Initializer{
        public static TraficQuickResponseComponent init(Application application){
            return DaggerTraficQuickResponseComponent.builder()
                    .applicationModule(new ApplicationModule(application))
                    .build();
        }
    }
}
